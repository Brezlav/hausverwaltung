<?php
require_once "loader.php";

// Form-Submit - Login
$errormsg = "";
$referer = "";
if (isset($_SERVER["HTTP_REFERER"])) {
  $referer = $_SERVER["HTTP_REFERER"];
}
if (count($_POST) > 0) {
  $myPDO = db_connect();
  $login_return = $auth->login($myPDO, $_POST["user"], $_POST["pwd"]);
  if ($login_return === true) {
    if ($_POST["from_url"] != "") header("location: " . $_POST["from_url"]);
    else header("location: " . BASEURL . "index.php");
    exit;
  }
  else {
    $errormsg = $login_return;
    $referer = $_POST["from_url"];
  }
}

require BASEPATH . "templates/header.php";
?>

<div class="container">

  <?php if ($errormsg != "") { ?>
    <div class="alert alert-danger">
      <b><?php echo $errormsg; ?></b>
    </div>
  <?php } ?>

	<meta charset="UTF-8">
	<title>Gemeinderat Protokoll</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
<div class="container">
	<div class="col-md-4 col-md-offset-4">
		<section>
			<div class="panel panel-default top caja">
			  <div class="panel-body">
			    <h3 class="text-center">Gemeinderat Protokolle</h3>
			    
			    <form action="" method="post">
			    <input type="hidden" name="from_url" value="<?php echo $referer; ?>">
			    	<div class="input-group input-group-lg">
					  <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-user" aria-hidden="true"></i></span>
					  <input type="text" name="user" id="user" class="form-control" placeholder="Username" aria-describedby="sizing-addon1" required>
					</div>
					<br>
					<div class="input-group input-group-lg">
					  <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-key" aria-hidden="true"></i></span>
					  <input type="password" name="pwd" id="pwd" class="form-control" placeholder="Passwort" aria-describedby="sizing-addon1" required>
					</div>
					<br>
					 <button type="submit" class="btn btn-primary btn-block">Login</button>

			    </form>
			  </div>
			</div>
		</section>
	</div>
	
</div>
</body>
<?php
require BASEPATH . "/templates/footer.php"
?>