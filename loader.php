<?php
define("BASEPATH", __DIR__ . "/");
define("BASEURL", "/");

require_once BASEPATH . "/inc/config.inc.php";
require_once BASEPATH . "/inc/helpers.inc.php";

spl_autoload_register(function ($classname) {
  $filename = BASEPATH . str_replace("\\", "/", $classname) . ".class.php";
  if (file_exists($filename)) {
    require_once $filename;

  }
  else {
    die("Fatal Error - Autoload failed!");
  }
});

$auth = new \Myclasses\Auth;
$db = new \Myclasses\db;
