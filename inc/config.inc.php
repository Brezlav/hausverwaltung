<?php

defined("BASEPATH") or die("No direct script access allowed");


const MYCONFIG = [
  "DBHOST"     => "localhost",
  "DBUSER"     => "root",
  "DBPASS"     => "1234",
  "DBDATABASE" => "organisator"];
  

// Errormessage

$insert_error 	= "<strong> Das Hochladen des Protokolls schlug fehl! </strong>";
$select_error 	= "<strong> Es konnten keine Protokolle gefunden werden! </strong>";
$delete_error	= "<strong> Das Löschen des Inhaltes schlug fehl!</strong>";
$db_error		= "<strong> Es konnte keine Verbindung zur Datenbank aufgebaut werden! </strong>";
$login_error 	= "<strong> Der Login schlug fehl! </strong>";
$role_error		= "<strong> Sie haben leider keine Berechtigung! </strong>";

?>
