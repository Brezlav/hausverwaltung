<?php
/**
 * Creates DB-Connection
 *
 * @throws PDOException on Error
 * @return PDO Object
 */
function db_connect() : PDO {
  $myPDO = new PDO("mysql:host=" . MYCONFIG["DBHOST"] . ";dbname=" . MYCONFIG["DBDATABASE"] . ";charset=utf8mb4" , MYCONFIG["DBUSER"], MYCONFIG["DBPASS"]);
  $myPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $myPDO->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
  return $myPDO;
}
