<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Gemeinderat Protokoll</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  </head>
<body>
<div class="container">
	<div class="row">
    
    <p></p>
    <h1>Bootstrap Table Panel with Pagination</h1>
    <p>A simple example of how-to put a bordered table within a panel. Responsive, place holders in header/footer for buttons or pagination.</p>
    <p>Follow me <a href="https://twitter.com/asked_io" target="_new">@asked_io</a> & <a href="https://asked.io/" target="_new">asked.io</a>.</p>
    <p> </p><p> </p>

    
    <form id="defaultForm" method="post" class="form-horizontal" action="upload_class.php">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Titel</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control" name="titel" placeholder="Titel" required>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="col-lg-3 control-label">File</label>
                            <div class="col-lg-4">
                                <input type="file" class="form-control" name="File" required>
                                <span class="help-block">Wähle eine PDF Datei aus.</span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Ausschuss</label>
                            <div class="col-lg-5">
                                <select class="form-control" name="ausschuss" required>
                                    <option value="">-- Bitte Auswählen --</option>
                                    <option value="gemeinderat">Gemeinderat</option>
                                    <option value="gemeindevorstand">Gemeindevorstand</option>
                                    <option value="bau">Bau- und Ortsentwicklungsausschuss</option>
                                    <option value="finanz">Finanz-, Verwaltungs- und Sicherheitssausschuss</option>
                                    <option value="freizeit">Freizeitbetriebe- und Tourismusausschuss</option>
                                    <option value="sozial">Generationen-, Gesundheits-, Sozial- und Integrationsausschuss</option>
                                    <option value="infrastruktur">Infrastrukturausschuss</option>
									<option value="sport">Sport-, Kultur- und Eventausschuss</option>
									<option value="wirtschaft">Wirtschaft- und Umweltausschuss</option>
									<option value="pruefung">Prüfungsausschuss</option>
									<option value="volksschule">Volksschulausschuss</option>
									<option value="mittelschule">Mittelschulausschuss</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-9 col-lg-offset-3">
                                <button type="submit" class="btn btn-primary" name="upload" value="upload">Datei absenden</button>
                               
                            </div>
                        </div>
    </form>
	</div>
</div>
</body>
</html>
