-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 25. Apr 2018 um 20:53
-- Server-Version: 5.7.21-0ubuntu0.16.04.1
-- PHP-Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `organisator`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `auschuss`
--

CREATE TABLE `auschuss` (
  `aid` int(11) NOT NULL,
  `bezeichnung` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `auschuss`
--

INSERT INTO `auschuss` (`aid`, `bezeichnung`) VALUES
(1, 'Gemeinderat'),
(2, 'Gemeindevorstand'),
(3, 'Bau- und Ortsentwicklung'),
(4, 'Finanz-, Verwaltungs-,Sicherheit'),
(5, 'Freizteitbetriebe und Tourismus'),
(6, 'Generation, Gesundheit, Sozial und Integration'),
(7, 'Infrastruktur'),
(8, 'Sport, Kultur und Event'),
(9, 'Wirtschaft und Umwelt'),
(10, 'Prüfungsauschuss'),
(11, 'Volksschule'),
(12, 'Mittelschule');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `protokoll`
--

CREATE TABLE `protokoll` (
  `pid` int(11) NOT NULL,
  `P_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `datei` blob,
  `auschuss` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `user_uplaod` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `protokoll`
--

INSERT INTO `protokoll` (`pid`, `P_name`, `datei`, `auschuss`, `status`, `user_uplaod`, `upload_date`) VALUES
(1, 'test', NULL, 1, 1, '1', '2018-04-25 16:43:54');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rollen`
--

CREATE TABLE `rollen` (
  `rid` int(10) UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `rollen`
--

INSERT INTO `rollen` (`rid`, `role`) VALUES
(1, 'admin'),
(2, 'vorstand'),
(3, 'gemeinderat'),
(4, 'mietglied');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `uid` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_host` varchar(255) NOT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `lastlogin_host` varchar(255) DEFAULT NULL,
  `aktiv` tinyint(1) NOT NULL,
  `user_roleid` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`uid`, `username`, `password`, `email`, `created`, `created_host`, `lastlogin`, `lastlogin_host`, `aktiv`, `user_roleid`) VALUES
(1, 'test', '$2y$10$bEu4gv4f3z7VRcGcff31ZO7CUDzvIpVZPcTy9dnEToc/2DgLCHLCO', 'test@test.at', '2018-03-14 20:53:56', '127.0.0.1', '2018-04-25 18:44:26', '127.0.0.1', 1, 1);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `auschuss`
--
ALTER TABLE `auschuss`
  ADD PRIMARY KEY (`aid`);

--
-- Indizes für die Tabelle `protokoll`
--
ALTER TABLE `protokoll`
  ADD PRIMARY KEY (`pid`);

--
-- Indizes für die Tabelle `rollen`
--
ALTER TABLE `rollen`
  ADD PRIMARY KEY (`rid`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`),
  ADD KEY `user_roleid` (`user_roleid`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `auschuss`
--
ALTER TABLE `auschuss`
  MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT für Tabelle `protokoll`
--
ALTER TABLE `protokoll`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `rollen`
--
ALTER TABLE `rollen`
  MODIFY `rid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `uid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`user_roleid`) REFERENCES `rollen` (`rid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
