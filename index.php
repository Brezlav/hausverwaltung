<?php 
require_once "loader.php";
require BASEPATH . "templates/header.php";
$auth->protect( [ "admin", "vorstand", "gemeinderat", "mitglied" ] );
?> 

    <meta charset="utf-8">
    <title>Gemeinderats Protokolle</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  </head>
<body>
<div class="container">
    <div class="row">
    
    <p></p>
    <h1>Protokollliste</h1>
    <p>Hier siehst du alle Protokolle welche mit deiner Berechtigung zugänglich sind!</p>
    <p>Bei Problemen <a href="http://www.zukunft-leobersdorf.at/" target="_new">homepage</a> & <a href="info@zukunft.at" target="_new">email</a>.</p>
    <p> </p><p> </p>
    
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default panel-table">
              <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h3 class="panel-title">Protokolle</h3>
                  </div>
                  <div class="col col-xs-6 text-right">
                    <a href="upload.php"><button type="button" class="btn btn-sm btn-primary btn-create">+ Neue Datei hochladen</button></a>
                  </div>
                </div>
              </div>
              <div class="panel-body">
                <table class="table table-striped table-bordered table-list">
                  <thead>
                    <tr>
                        <th><em class="fa fa-cog"></em></th>
                        <th class="hidden-xs">ID</th>
                        <th>Dateiname</th>
                        <th>Datum</th>
								<th>Abteilung</th>
                    </tr> 
                  </thead>
                  <tbody>
                  <tr><td align="center">
                  <?php 
                  $db->select_protokoll($results);
                  if ($_SESSION["$rid"] != "mitglied"){  
                  echo "
                     
         					<a href='edit.php?id= <?php $rid  ?>' class='btn btn-default'><em class='fa fa-pencil'></em></a>
         					<a href='delete.php?id= <?php $rid ?>' class='btn btn-danger'><em class='fa fa-trash'></em></a>
         					</td>";} 
         			?>
         					<td class="hidden-xs">1</td>
         					<td><?php $titel; ?></td>
         					<td><?php $date; ?></td>
         					<td><?php $auschuss; ?></td>
         					<td><?php $user; ?></td>
								<td></td>
    							</tr> 
                        </tbody>
                </table>
            </div>
              <div class="panel-footer">
                <div class="row">
                  <div class="col col-xs-4">Seite 1 of 2
                  </div>
                  <div class="col col-xs-8">
                    <ul class="pagination hidden-xs pull-right">
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                    </ul>
                    <ul class="pagination visible-xs pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
		</div>
	</div>
</div>
</body>
<?php require BASEPATH . "/templates/footer.php";