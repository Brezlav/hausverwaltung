<?php
namespace Myclasses;
/*
 Datenbank Klasse
	Select, Insert, Delete, Query
	
*/	 
class DB {
	
/**
Funktion zeigt Einträge aus Datenbank Protokolle

Rückgabewert: boolean, $titel, $date , $user, $rid, $auschuss


**/

  public function select_protokoll(\PDO $myPDO, string $titel, string $date,  string $user, string $rid, string $auschuss) {
    if ($this->has_role('mitglied') {
      $stmt = $myPDO->prepare("SELECT * FROM 'protokoll' 
      									JOIN 'user' ON 'rollen.rid' = 'user.roleid. 
											JOIN 'ausschuss' ON 'protokoll' = 'ausschuss.aid'      									
      									WHERE 'status' = IS NOT NULL");
      	$stmt->bindParam(':p_name', $titel);
			$stmt->bindParam(':username',$user );
			$stmt->bindParam(':upload_date',$date);
			$stmt->bindParam(':rid',$rid );
			$stmt->bindParam(':auschuss',$auschuss);
    } else {
      $stmt = $myPDO->prepare("SELECT * FROM 'protokoll' 
      									JOIN 'user' ON 'rollen.rid' = 'user.roleid. 
											JOIN 'ausschuss' ON 'protokoll' = 'ausschuss.aid'");
      	$stmt->bindParam(':p_name', $titel);
			$stmt ->bindParam(':username',$user );
			$stmt ->bindParam(':upload_date',$date);
			$stmt ->bindParam(':rid',$rid );
			$stmt->bindParam(':auschuss',$auschuss);
    }
      $stmt->execute();
      $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      if (empty($results)) {
        return false;
      } else {
        return $results;
        return true;
      }
  }

/**

Funktion löscht aus Datenbank Protokolle

Rückgabewert: boolean


**/

  public function delete_protokoll(\PDO $myPDO, string $table, string $id) {
    $stmt = $myPDO->prepare("DELETE FROM ".$table." WHERE `id`= ? ");
    $stmt->execute([$id]);
    if ($stmt->rowCount() > 0) {
          return true;
        } else {
          return false;
        }
  }

}
?>