<?php
namespace Myclasses;

/**
 * Litte Auth Class with login/logout/register
 */
class Auth {

  /**
   * ERROR-Messages
   * (maybe better: class-specific constants should be defined in the class... just return the defined error-msgs instead of error-codes)
   */
  const ERRORMSG = [
    "LOGINFAILED"           => "Login failed",
    "REGISTER_USER_EXISTS"  => "Username is already in use",
    "REGISTER_MAIL_EXISTS"  => "Mail address is already in use",
    "REGISTER_USER_INVALID" => "Please set a valid username",
    "REGISTER_MAIL_INVALID" => "Please set a valid e-mail",
    "REGISTER_PASS_INVALID" => "Please set a secure password"
  ];

  /**
   * Class-Settings
   */
  const REGISTER_DEFAULT_ROLE = "mitglied";
  const TMPL_LOGIN_FAIL = BASEPATH . "templates/perm_fail_login.php";
  const TMPL_ROLE_FAIL = BASEPATH . "templates/perm_fail_roles.php";
  const TMPL_FAIL_FOOTER = BASEPATH . "templates/footer.php";

  /**
   * Constructor: by default, we start the session
   *
   * @param boolean (optional) start session
   */
  public function __construct(bool $startsession = true) {
    if ($startsession) {
      session_start();
    }
  }

  /**
   * checks, if user is logged in AND has the role (if array is set)
   * if not, it includes the error-templates and EXIT the script for security!
   *
   * @param array (optional) if set, the list of roles, which are allowed
   */
  public function protect(array $ar = []) {
    if (!$this->is_login()) {
      include self::TMPL_LOGIN_FAIL;
      include self::TMPL_FAIL_FOOTER;
      exit;
    }
    elseif (!$this->has_role($ar)) {
      include self::TMPL_ROLE_FAIL;
      include self::TMPL_FAIL_FOOTER;
      exit;
    }
  }

  /**
   * checks, if user is logged in
   *
   * @return boolean
   */
  public function is_login() : bool {
    if (isset($_SESSION["uid"]) && $_SESSION["uid"] != "") {
      return true;
    }
    return false;
  }

  /**
   * checks, if user has one of the defined roles (via parameter, if set)
   *
   * @param array (optional) if set, the list of roles, which are allowed
   * @return boolean
   */
  public function has_role(array $ar = []) : bool {
    if (is_array($ar)) {
      if (count($ar) === 0) {
        return true;
      }
      elseif (in_array($_SESSION["role"], $ar)) {
        return true;
      }
    }
    return false;
  }

  /**
   * login method
   * @return boolean true if ok
   * @return string errormsg if not ok
   */
  public function login(\PDO $db, string $username, string $password) {
    $stmt = $db->prepare("SELECT uid, username, password, email, role
                                FROM user
                                  JOIN rollen
                                  ON user_roleid = rid
                                WHERE (username = :username OR email = :username)
                                  AND aktiv = 1");
    $stmt->bindParam(':username', $username);
    $stmt->execute();
    if ($row = $stmt->fetch()) {
      if (password_verify($password, $row["password"])) {
        $_SESSION["uid"] = $row["uid"];
        $_SESSION["role"] = $row["role"];
        $db->exec("UPDATE user SET lastlogin = NOW(), lastlogin_host = '" . $_SERVER["REMOTE_ADDR"] . "' WHERE uid = ".$row["uid"]);
        return true;
      }
    }
    return self::ERRORMSG["LOGINFAILED"];
  }

  /**
   * logout method
   * destroy session and delete session-cookie!
   */
  public function logout() {
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }
    $_SESSION = array();
    session_destroy();
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
  }

  /**
   * register new user (and login)...
   * @return boolean true, when ok
   * @return array of strings with error-messages, when not ok
   */
  public function register(\PDO $db, string $username, string $email, string $password) {
    $errors = [];
    if (!isset($username) || trim($username) == "" || filter_var($username, FILTER_VALIDATE_EMAIL) === true)
      $errors[] = self::ERRORMSG["REGISTER_USER_INVALID"];
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === false)
      $errors[] = self::ERRORMSG["REGISTER_MAIL_INVALID"];
    if (!isset($password) || strlen(trim($password)) < 4)
      $errors[] = self::ERRORMSG["REGISTER_PASS_INVALID"];

    $stmt = $db->prepare("SELECT uid FROM user WHERE username = ?");
    $stmt->execute([$username]);
    if ($stmt->rowCount() > 0)
      $errors[] = self::ERRORMSG["REGISTER_USER_EXISTS"];

    $stmt = $db->prepare("SELECT uid FROM user WHERE email = ?");
    $stmt->execute([$email]);
    if ($stmt->rowCount() > 0)
      $errors[] = self::ERRORMSG["REGISTER_MAIL_EXISTS"];

    if (count($errors) === 0) {
      $get_default_role = $db->query("SELECT rid FROM rollen WHERE role = '" . self::REGISTER_DEFAULT_ROLE . "'");
      if ($role_row = $get_default_role->fetch()) {
	$stmt = $db->prepare("INSERT INTO user (username, password, email, created_host, aktiv, user_roleid) VALUES (:username, :password, :email, :host, 1, :roleid)");
	$stmt->execute([
	  ":username" => $username,
	  ":password" => password_hash($password, PASSWORD_BCRYPT),
	  ":email"    => $email,
	  ":host"     => $_SERVER["REMOTE_ADDR"],
	  ":roleid"   => $role_row["rid"]
	]);
	$this->login($db, $username, $password);
	return true;
      }
      else {
	$errors[] = "undefined error - default role not found!";
      }
    }
    return $errors;
  }
}
